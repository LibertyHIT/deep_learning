import torch.nn as nn


class VGG11(nn.Module):
    def __init__(self, use_lrn=False):
        """
        初始化VGG11网络
        :param use_lrn: 是否使用LRN
        """
        super(VGG11, self).__init__()
        self.use_lrn = use_lrn
        self.conv1 = nn.Sequential(
            nn.Conv2d(
                in_channels=3,
                out_channels=64,
                kernel_size=3,
                padding=1
            ),
            nn.ReLU(inplace=True)
        )
        if use_lrn:
            self.lrn = nn.LocalResponseNorm(4)
        self.pool1 = nn.MaxPool2d(
            kernel_size=2,
            stride=2,
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(
                in_channels=64,
                out_channels=128,
                kernel_size=3,
                padding=1
            ),
            nn.MaxPool2d(
                kernel_size=2,
                stride=2
            )
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(
                in_channels=128,
                out_channels=256,
                kernel_size=3,
                padding=1,
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=256,
                out_channels=256,
                kernel_size=3,
                padding=1,
            ),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(
                kernel_size=2,
                stride=2,
            )
        )
        self.conv4 = nn.Sequential(
            nn.Conv2d(
                in_channels=256,
                out_channels=512,
                kernel_size=3,
                padding=1,
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=512,
                out_channels=512,
                kernel_size=3,
                padding=1,
            ),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(
                kernel_size=2,
                stride=2,
            )
        )
        self.conv5 = nn.Sequential(
            nn.Conv2d(
                in_channels=512,
                out_channels=512,
                kernel_size=3,
                padding=1,
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=512,
                out_channels=512,
                kernel_size=3,
                padding=1,
            ),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(
                kernel_size=2,
                stride=2,
            )
        )
        self.fc = nn.Linear(
            in_features=512,
            out_features=10
        )
        # 对每层参数进行初始化
        for layer in self.modules():
            if isinstance(layer, nn.Conv2d):
                nn.init.kaiming_normal_(layer.weight, mode='fan_out', nonlinearity='relu')
                if layer.bias is not None:
                    nn.init.constant_(layer.bias, 0.)
            elif isinstance(layer, nn.Linear):
                nn.init.normal_(layer.weight, 0.01, 0.1)
                nn.init.constant_(layer.bias, 0.)

    def forward(self, x):
        x = self.conv1(x)
        if self.use_lrn:
            x = self.lrn(x)
        x = self.pool1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = x.view(x.shape[0], 512)
        x = self.fc(x)
        return x
