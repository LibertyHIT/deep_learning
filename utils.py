import torch.utils.data as data
from torch.utils.data import DataLoader
from torch import optim
import os
import numpy as np
import pickle
import torch
import torch.nn as nn
import random
from tensorboardX import SummaryWriter
from VGGNet import VGG11
# 初始化训练参数
BATCH_SIZE = 128
EPOCH = 5


class Cifar10(data.Dataset):
    def __init__(self, root, train=True):
        """
        初始化Cifar-10数据集
        :param root: 数据集根目录 应包含 data_batch_1 ~ data_batch_5 以及 test_batch
        :param train: 读取的是否为训练集数据
        """
        self.root = root
        self.train = train
        self.x, self.y = self.load_data()

    def __len__(self):
        """
        得到数据的总长度
        :return: 数据总长度 train: 50000 / test: 10000
        """
        if self.train:
            return 50000
        else:
            return 10000

    def __getitem__(self, index):
        """
        得到某组数据及标签
        :param index: 索引
        :return: 对应索引的数据及标签
        """
        return self.x[index], self.y[index]

    def load_data(self):
        """
        将Cifar-10数据集读取为np.ndarray
        :return:
                数据: np.float32
                标签: np.long
        """
        def load_batch(filename):
            """
            读取某个batch的数据
            :param filename: batch文件名
            :return: 该batch的数据及标签
            """
            with open(filename, 'rb') as f:
                datadict = pickle.load(f, encoding='latin1')
                b_x = datadict['data']
                b_y = datadict['labels']
                b_x = b_x.reshape(10000, 3, 32, 32).astype("float")
                b_y = np.array(b_y)
                return b_x, b_y
        if self.train:
            xs = []
            ys = []
            for i in range(1, 6):
                batch_filename = os.path.join(self.root, 'data_batch_%d' % i)
                x, y = load_batch(batch_filename)
                xs.append(x)
                ys.append(y)
            x = np.concatenate(xs)
            y = np.concatenate(ys)
        else:
            x, y = load_batch(os.path.join(self.root, 'test_batch'))
        return x.astype(np.float32), y.astype(np.long)


def test_model(cur_model, dataset_folder, test_loader=None, use_gpu=False):
    """
    模型测试
    :param cur_model: 当前使用的模型
    :param dataset_folder: 数据集目录
    :param test_loader: 测试集的DataLoader
    :param use_gpu: 是否使用GPU
    :return: 测试正确率
    """
    if not test_loader:
        test_data = Cifar10(dataset_folder, False)
        test_loader = DataLoader(
            dataset=test_data,
            batch_size=BATCH_SIZE,
            shuffle=False,
        )
    # 改变模型模式为eval
    cur_model.eval()
    all_correct = 0.0
    with torch.no_grad():
        for _, (test_x, test_y) in enumerate(test_loader):
            if use_gpu:
                test_x = test_x.cuda()
                test_y = test_y.cuda()
            ts_output = cur_model(test_x)
            pred_ts_y = torch.max(ts_output, -1)[1]
            all_correct += (pred_ts_y == test_y).sum().item()
            if use_gpu:
                torch.cuda.empty_cache()
        acc = float(all_correct) / 10000.0
    return acc


def train_model(cur_model, dataset_folder, optimizer, writer_path, use_gpu=True, save_path=None):
    """
    模型训练
    :param cur_model: 当前使用的模型
    :param dataset_folder: 数据集目录
    :param optimizer: 当前使用的优化器
    :param writer_path: SummaryWriter绘制的路径
    :param use_gpu: 是否使用GPU
    :param save_path: 模型保存的路径
    :return: None
    """
    writer = SummaryWriter()
    # 初始化数据集
    train_data = Cifar10(dataset_folder, True)
    train_loader = DataLoader(
        dataset=train_data,
        batch_size=BATCH_SIZE,
        shuffle=True
    )
    test_data = Cifar10(dataset_folder, False)
    test_loader = DataLoader(
        dataset=test_data,
        batch_size=BATCH_SIZE,
        shuffle=False,
    )
    # 定义损失函数
    criterion = nn.CrossEntropyLoss()
    if use_gpu:
        criterion.cuda()
    global_step = 0
    for epoch in range(EPOCH):
        print_loss = None
        for step, (x, y) in enumerate(train_loader):
            cur_model.train()
            optimizer.zero_grad()
            if use_gpu:
                x = x.cuda()
                y = y.cuda()
            output = cur_model(x)
            loss = criterion(output, y)
            loss.backward()
            print_loss = loss.float().item()
            writer.add_scalar(writer_path + 'loss/', print_loss, global_step)
            global_step += 1
            optimizer.step()
            torch.cuda.empty_cache()
            cur_model.eval()
            if step % 50 == 0 and step:
                acc = test_model(cur_model, None, test_loader, use_gpu)
                writer.add_scalar(writer_path + 'test acc', acc, global_step)
                print("Step: ", step, "\t|\tLoss: ", print_loss, "\t|\tTest Acc: ", acc)
        acc = test_model(cur_model, None, test_loader, use_gpu)
        print("Epoch: ", epoch, "\t|\tLoss: ", print_loss, "\t|\tTest Acc: ", acc)
    if save_path is not None:
        torch.save(cur_model.state_dict(), save_path)


def setup_seed(seed):
    """
    设置随机种子
    :param seed: 种子
    :return: None
    """
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    torch.backends.cudnn.deterministic = True


def test_lr(lrs=(1e-2, 5e-3, 1e-3, 5e-4, 1e-4), use_gpu=True):
    """
    测试5个EPOCH下不同学习率下VGG使用Adam训练效果
    :param lrs: 测试使用的学习率
    :param use_gpu: 是否使用GPU
    :return: None
    """
    for lr in lrs:
        # 初始化种子
        setup_seed(0)
        model = VGG11()
        if use_gpu:
            device = torch.device(0)
        else:
            device = torch.device('cpu')
        model.to(device)
        opt = optim.Adam(model.parameters(), lr=lr)
        train_model(model, './cifar-10/cifar-10-batches-py/', opt, 'test_lr/', use_gpu, None)


def test_optimizer(use_gpu=True):
    """
    测试5个EPOCH下VGG以5e-4的学习率分别使用SGD，Adam的训练效果
    :param use_gpu: 是否使用GPU
    :return: None
    """
    for i in range(2):
        setup_seed(0)
        model = VGG11()
        if use_gpu:
            device = torch.device(0)
        else:
            device = torch.device('cpu')
        model.to(device)
        if i == 0:
            opt = optim.Adam(model.parameters(), lr=5e-4)
        else:
            opt = optim.SGD(model.parameters(), lr=5e-4)
        train_model(model, './cifar-10/cifar-10-batches-py/', opt, 'test_optimizer/', use_gpu, None)

