import argparse
from ResNet import ResNet18
from utils import *
from torch import optim
torch.backends.cudnn.enabled = True


if __name__ == '__main__':
    # 实例化一个Parser对象
    parser = argparse.ArgumentParser()
    model_group = parser.add_mutually_exclusive_group(required=True)
    #添加参数
    model_group.add_argument('-v', '--vgg', action='store_true', help='use VGGNet')
    model_group.add_argument('-r', '--resnet', action='store_true', help='use ResNet')
    parser.add_argument('-lr', '--learning_rate', help='set the learning rate', action='store', default=5e-4)
    mode_group = parser.add_mutually_exclusive_group(required=True)
    mode_group.add_argument('-train', '--train', action='store_true', help='train the model')
    mode_group.add_argument('-test', '--test', action='store_false', dest='train', help='test the model')
    parser.add_argument('-p', '--path', help='choose the path to save/load the model',required=True)
    device_group = parser.add_mutually_exclusive_group(required=False)
    device_group.add_argument('-c', '--cpu', action='store_false', dest='gpu', help='use cpu to train/test')
    device_group.add_argument('-g', '--gpu', action='store_true', help='use gpu to train/test', default=True)
    optimizer_group = parser.add_mutually_exclusive_group(required=False)
    optimizer_group.add_argument('-a', '--adam', action='store_true', help='use adam to optimize', default=True)
    optimizer_group.add_argument('-s', '--sgd', action='store_false', dest='adam', help='use sgd to optimize')
    parser.add_argument('-dp', '--dataset_path', help='choose the path of cifar-10', required=True,
                        default='./cifar-10')
    args = parser.parse_args()
    setup_seed(0)
    # 选择模型
    if args.vgg:
        model = VGG11()
        w_path = 'vgg/'
    else:
        model = ResNet18()
        w_path = 'resnet/'
    # 选择硬件
    if args.gpu:
        device = torch.device(0)
    else:
        device = torch.device('cpu')
    model.to(device)
    LR = float(args.learning_rate)
    # 选择优化方法
    if args.adam:
        opt = optim.Adam(model.parameters(), lr=LR)
        w_path += 'adam/LR:%s/' % args.learning_rate
    else:
        opt = optim.SGD(model.parameters(), lr=LR)
        w_path += 'sgd/LR:%s/' % args.learning_rate
    # 选择模式
    if args.train:
        train_model(model, args.dataset_path, opt, w_path, args.gpu, args.path)
    else:
        if args.path:
            model.load_state_dict(torch.load(args.path, map_location=device))
            acc = test_model(model, args.dataset_path, args.gpu)
            print('Prediction Accuracy: %4f' % acc)
        else:
            train_model(model, args.dataset_path, opt, w_path, args.gpu, args.path)
