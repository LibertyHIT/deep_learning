import torch.nn as nn
import torch.nn.functional as F


class ResBlock(nn.Module):
    def __init__(self, in_channel, out_channel):
        """
        初始化ResNet的子模块
        :param in_channel: 输入通道数
        :param out_channel: 输出通道数
        """
        super(ResBlock, self).__init__()
        self.bn2 = nn.BatchNorm2d(out_channel)
        self.bn3 = nn.BatchNorm2d(out_channel)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(
            in_channels=in_channel,
            out_channels=out_channel,
            stride=2,
            padding=1,
            kernel_size=3
        )
        self.conv3 = nn.Conv2d(
            in_channels=out_channel,
            out_channels=out_channel,
            padding=1,
            kernel_size=3,
            stride=1
        )
        self.shortcut = nn.Sequential(
            nn.ConstantPad3d((0, 0, 0, 0, out_channel - in_channel, 0), 0.),
            nn.MaxPool2d(
                kernel_size=1,
                stride=2,
            )
        )

    def forward(self, x):
        """
        前向传播
        :param x: 输入向量
        :return: 前向传播结果
        """
        x2 = self.conv2(x)
        x2 = self.bn2(x2)
        x2 = self.relu(x2)
        x2 = self.conv3(x2)
        x2 = self.bn3(x2)
        bypass = self.shortcut(x)
        x2 = self.relu(bypass + x2)
        return x2


class ResNet18(nn.Module):
    def __init__(self):
        """
        ResNet18初始化
        """
        super(ResNet18, self).__init__()
        self.block1 = nn.Sequential(
            # 卷积层
            nn.Conv2d(
                in_channels=3,
                out_channels=64,
                kernel_size=3,
                stride=1,
                padding=1,
            ),
            nn.BatchNorm2d(64),
            # Relu激活函数
            nn.ReLU(inplace=True),
        )
        self.block2 = nn.Sequential(
            nn.Conv2d(
                in_channels=64,
                out_channels=64,
                kernel_size=3,
                padding=1
            ),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=64,
                out_channels=64,
                kernel_size=3,
                padding=1
            ),
            nn.BatchNorm2d(64)
        )
        self.block3 = ResBlock(64, 128)
        self.block4 = ResBlock(128, 256)
        self.block5 = ResBlock(256, 512)
        self.block6 = nn.Sequential(
            nn.Linear(
                in_features=512,
                out_features=10
            )
        )
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        """
        使用之前定义的网络结构前向传播
        :param x: 输入向量
        :return: 前向传播结果
        """
        x1 = self.block1(x)
        x2 = self.block2(x1) + x1
        x2 = self.relu(x2)
        x2 = self.block3(x2)
        x2 = self.block4(x2)
        x2 = self.block5(x2)
        x2 = F.avg_pool2d(x2, 4)
        x2 = x2.view(x2.shape[0], 512)
        x2 = self.block6(x2)
        return x2
